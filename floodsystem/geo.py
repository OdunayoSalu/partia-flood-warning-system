# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module contains a collection of functions related to
geographical data.

"""

from .utils import sorted_by_key  # noqa
from haversine import haversine

def stations_by_distance(stations, p):
    ordered = []

    for i in stations:
        ordered.append((i.name, haversine(i.coord, p)))

    return sorted_by_key(ordered, 1)


def rivers_with_stations(stations):
    
    return {i.river for i in stations}


def stations_by_river(stations):
    
    river_dict = {}

    for i in rivers_with_stations(stations):
        river_dict[i] = [x.name for x in stations if x.river == i]

    return river_dict
        
def stations_within_radius(stations, centre, r):
    newList = []
    
    ordered_stations = stations_by_distance(stations,centre)
    print ("Stations in range:")
    
    runningCondition = True
    loopCounter = 0
    while runningCondition:
        if ordered_stations[loopCounter][1] > r:
            runningCondition = False
        else:
            newList.append(ordered_stations[loopCounter])
            loopCounter = loopCounter +1
     
    return newList 

def rivers_by_station_number(stations, N):
    river_station_dict = stations_by_river(stations)   

    def station_count(river):
        return len(river_station_dict[river])

    sorted_rivers = sorted(list((river_station_dict.keys())), key = station_count, reverse = True)

    sorted_rivers_with_stations = [(i, len(river_station_dict[i])) for i in sorted_rivers]

    while sorted_rivers_with_stations[N-1][1] == sorted_rivers_with_stations[N][1]:
        N += 1
    
    return sorted_rivers_with_stations[:N]

    