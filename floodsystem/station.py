# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module provides a model for a monitoring station, and tools
for manipulating/modifying station data

"""


class MonitoringStation:
    """This class represents a river level monitoring station"""

    def __init__(self, station_id, measure_id, label, coord, typical_range,
                 river, town):

        self.station_id = station_id
        self.measure_id = measure_id

        # Handle case of erroneous data where data system returns
        # '[label, label]' rather than 'label'
        self.name = label
        if isinstance(label, list):
            self.name = label[0]

        self.coord = coord
        self.typical_range = typical_range
        self.river = river
        self.town = town

        self.latest_level = None

    def __repr__(self):
        d = "Station name:     {}\n".format(self.name)
        d += "   id:            {}\n".format(self.station_id)
        d += "   measure id:    {}\n".format(self.measure_id)
        d += "   coordinate:    {}\n".format(self.coord)
        d += "   town:          {}\n".format(self.town)
        d += "   river:         {}\n".format(self.river)
        d += "   typical range: {}".format(self.typical_range)
        return d
    
    def typical_range_consistent(self):
        a = self.typical_range

        if type(a) != tuple or len(a) != 2:
            return False
        elif a[0] > a[1]:
            return False
        elif a[0] < a[1]:
            return True
    
    def relative_water_level(self):

        # Fetch level data
        measure_data = datafetcher.fetch_latest_water_level_data()
    
        # Build map from measure id to latest reading (value)
        measure_id_to_value = dict()
        for measure in measure_data['items']:
            if 'latestReading' in measure:
                latest_reading = measure['latestReading']
                measure_id = latest_reading['measure']
                measure_id_to_value[measure_id] = latest_reading['value']
    
        #Fetch Typical Water Level Data
        typical_low = station.typical_range[0]
        typical_high = station.typical_range[1]
        relative_value = typical_low / typical_high [i]
        return relative_value
        
    def stations_level_over_threshold(stations, tol):
        ordered = []
    
        for i in stations:
            if relative_water_level >= tol
                ordered.append((i.name, relative_water_level(self)))
            
    return sorted_by_key(ordered, 1)


def inconsistent_typical_range_stations(stations):
    return [x.name for x in stations if x.typical_range_consistent() == False]