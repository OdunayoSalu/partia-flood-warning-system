def relative_water_level(self):

    # Fetch level data
    measure_data = datafetcher.fetch_latest_water_level_data()

    # Build map from measure id to latest reading (value)
    measure_id_to_value = dict()
    for measure in measure_data['items']:
        if 'latestReading' in measure:
            latest_reading = measure['latestReading']
            measure_id = latest_reading['measure']
            measure_id_to_value[measure_id] = latest_reading['value']

    #Fetch Typical Water Level Data
    typical_low = station.typical_range[0]
    typical_high = station.typical_range[1]
    relative_value = typical_low / typical_high [i]
    return relative_value

    #low/high
    for i in measure_data[i]:
        relative_value = typical_low[i] / typical_high [i]
        return relative_value


    


def stations_level_over_threshold(stations, tol):
    ordered = []

    for i in stations:
        ordered.append((i.name, relative_water_level(self)))
        
    return sorted_by_key(ordered, 1)