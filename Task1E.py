from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_by_station_number

def run():

    #Buil list of stations
    stations = build_station_list()

    print("The rivers with the 9 most stations are:")
    for i in rivers_by_station_number(stations, 9):
        print (i)


if __name__ == "__main__":
    print("*** Task 1C: CUED Part IA Flood Warning System ***")
    run()
