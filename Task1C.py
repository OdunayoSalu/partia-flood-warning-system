from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_within_radius

def run():

    #Buil list of stations
    stations = build_station_list()

    #list of stations within the radius
    valid_stations = stations_within_radius(stations, (52.2053, 0.1218), 10)

    #orders the station alphabetically
    #ordered_stations = valid_stations.sort()

    print("The stations in range are:")
    print(valid_stations)


if __name__ == "__main__":
    print("*** Task 1C: CUED Part IA Flood Warning System ***")
    run()

