from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_by_distance

def run():
    """Requirements for Task 1A"""

    # Build list of stations
    stations = build_station_list()

    ordered_stations = stations_by_distance(stations,(52.2053, 0.1218))
    print ("10 closest stations to Cambridge are:")

    for i in ordered_stations[:10]:
        print (i)

    print ("and 10 furthest are:")
    for i in ordered_stations[-10:]:
        print (i)

if __name__ == "__main__":
    print("*** Task 1A: CUED Part IA Flood Warning System ***")
    run()



