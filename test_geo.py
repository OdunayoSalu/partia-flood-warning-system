from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_by_distance
from floodsystem.geo import rivers_with_stations
from floodsystem.geo import stations_by_river

stations = build_station_list()

def test_stations_by_distance(stations):
    a = stations_by_distance(stations, (52.2074411, 0.12584489999999998)) 

    #Check that all items in a are 2 item tuples with item 1 being a string and item 2 being a float
    #and that the list is sorted closest station first etc
    for i in a:
        assert len(i) == 2
        assert type(i[0]) == str
        assert type(i[1]) == float

        if a.index(i) != len(a)-1:
            assert i[1] <= a[a.index(i)+1][1]


def test_rivers_with_station(stations):
    a = rivers_with_stations(stations)
    
    #Check that every river from the stations list shows up in our list of rivers
    for i in stations:
        assert i.river in a


def test_stations_by_river(stations):
    a = stations_by_river(stations)

    #Check that every station in a given dictionary value list truly corresponds to the river in the key
    #Had to account for the fact that there are some stations with the same name
    for i in a:
        for j in a[i]:
            #Makes a list of all rivers belonging to stations with name 'j'
            l = [m.river for m in stations if m.name == j]
            assert i in l
            
            
            
def test_rivers_by_station_number(stations):
    a = rivers_by_station_number(stations, 9)

    #check that the result shows the river's name followed by the number of stations corresponding to each station
    for i in a:
        assert type(i[0]) == str
        assert type(i[1]) == int
        assert len(i) == 2
        
        

def test_stations_within_radius(stations):
    b = stations_within_radius(stations, (52.2053, 0.1218), 10)

    #check that the output shows the cities
    for i in b:
        assert type(i[0]) == str
