from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_with_stations
from floodsystem.geo import stations_by_river

def run():
    """Requirements for Task 1A"""

    # Build list of stations
    stations = build_station_list()

    rivers = rivers_with_stations(stations)

    print(len(rivers))

    river_list = sorted(list(rivers))
    
    print(river_list[:10])

    for i in ["River Aire", "River Cam", "River Thames"]:
        print("\nStations located on " + i + ":")
        print(sorted(stations_by_river(stations)[i]))

if __name__ == "__main__":
    print("*** Task 1A: CUED Part IA Flood Warning System ***")
    run()